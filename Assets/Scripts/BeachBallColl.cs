﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeachBallColl : MonoBehaviour {
    public int collAmount = 5;

    public AudioClip BBCClip;
    public AudioSource BBCSource;

    void Start()
    {
        BBCSource.clip = BBCClip;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        BBCSource.Play();

        //If the player collides with this object, decrease stamina.
        ScoreManager.stamina = ScoreManager.stamina - collAmount;

    }
}
