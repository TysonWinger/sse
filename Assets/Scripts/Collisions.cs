﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour {
    public int collAmount = 5;

    public AudioClip CollisionClip;
    public AudioSource CollisionSource;

    public void Start()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        AudioSource.PlayClipAtPoint(CollisionClip, transform.position);

        //If the player collides with this object, decrease stamina.
        ScoreManager.stamina = ScoreManager.stamina - collAmount;


        Destroy(gameObject);
    }
}
