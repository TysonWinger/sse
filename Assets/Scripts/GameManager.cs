﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    //UI
    public Text tScoreBox;
    public Text tStaminaBox;

    public Text tPauseMenuBox;

    private static int totalPoints;

    //Vars
    public static int fPoints;
    float time = 0.1f;

    private void Start()
    {
        //Increase points over time.
        InvokeRepeating("PointsUp", time, time);
        fPoints = 0;
        tPauseMenuBox.enabled = false;
    }

    // LateUpdate is called once per frame, after Update
    void Update()
    {
        
      //Makes the score go up as long as the game is going
      tScoreBox.text = "Score: " + fPoints;
      tStaminaBox.text = "Stamina: " + ScoreManager.stamina;

        //pause stuff.
        tPauseMenuBox.text = "-Paused-";

        //Stop increasing the points if character dies.
        if (ScoreManager.stamina < 1)
        {
            totalPoints = totalPoints + fPoints;
            CancelInvoke();
        }

        if (Pause.bPause == true)
        {
            tPauseMenuBox.enabled = true;
        }
        else
        {
            tPauseMenuBox.enabled = false;
        }

    }

    public void PointsUp()
    {
        fPoints++;
    }
}

