﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour {

    //This audio doesn't work.

    public AudioSource crunchSource;
    public AudioClip crunchClip;

    private void Start()
    {
        crunchSource.clip = crunchClip;

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Crunch");

        //Chomp chomp, increase stamina.
        ScoreManager.stamina = ScoreManager.stamina + 5;


        //Increase points.
        GameManager.fPoints = GameManager.fPoints + 50;

        AudioSource.PlayClipAtPoint(crunchClip, gameObject.transform.position);

        Destroy(gameObject);

    }

}
