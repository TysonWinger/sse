﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destruction : MonoBehaviour {

    public GameObject destructionPoint;

    public void Start()
    {
        destructionPoint = GameObject.Find("TreeDestructionPoint");
    }

    // Update is called once per frame
    void Update () {
		if ( transform.position.x < destructionPoint.transform.position.x)
        {
            Destroy(gameObject);
        }
	}
}
