﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGTreeGenerator : MonoBehaviour {

    public GameObject tree;
    public Transform generationPoint;



    private float distanceBetweenTrees;
    public int highRange;
    public int lowRange;
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < generationPoint.transform.position.x)
        {
            distanceBetweenTrees = Random.Range(lowRange, highRange);

            transform.position = new Vector3(transform.position.x + distanceBetweenTrees, transform.position.y, transform.position.z);

            Instantiate(tree, transform.position, transform.rotation);
        }
	}
}
