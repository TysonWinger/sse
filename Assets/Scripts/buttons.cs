﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour {

    //Open main menu.
    public void MainMenuBtn (string MainMenu)
    {
        SceneManager.LoadScene(MainMenu);
    }

    //Open game.
    public void RetryBtn (string SSE)
    {
        SceneManager.LoadScene(SSE);
    }
}
