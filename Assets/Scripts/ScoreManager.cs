﻿using System.Collections;
using UnityEngine;

public class ScoreManager : MonoBehaviour {
    //Vars
    public static int stamina;
    public static int maxStamina = 25;
    public float Counter = 2f;

    public Animator scoreAnim;

    
    public void Start()
    {
        //Starting Stamina
        stamina = 25;

        //Keep reducing stamina at the counter value.
        InvokeRepeating("StaminaReduction", Counter, Counter);

        scoreAnim = GetComponent<Animator>();
    }

    public void FixedUpdate()
    {
        scoreAnim.SetInteger("StaminaValue", stamina);

        //Cap stamina.
        if (stamina > maxStamina)
        {
            stamina = maxStamina;
        }

        //Upon death, freeze stamina.
        if (stamina < 1)
        {
            stamina = 0;
            CancelInvoke();
        }


    }
        



        //Function for reducing stamina.
    public void StaminaReduction()
    {
            stamina --;
    }
}
