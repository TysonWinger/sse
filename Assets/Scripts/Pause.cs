﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour{

    public static bool bPause = false;

    void Update()
    {

        if (bPause == false)
        {
            Time.timeScale = 1;
        }

        else
        {
            Time.timeScale = 0;
        }


        if (Input.GetKeyDown(KeyCode.P) || (Input.GetKeyDown(KeyCode.Escape)))
        {
            if (bPause == true)
            {
                bPause = false;
            }

            else
            {
                bPause = true;
            }
        }
    }
}