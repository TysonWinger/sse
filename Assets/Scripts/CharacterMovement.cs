﻿using UnityEngine;
using UnityEngine.Audio;

public class CharacterMovement : MonoBehaviour
{
    //Vars
    public float speed = 10f;
    public float jumpSpeed = 12f;
    public float raycastDistance = 0.1f;
    public float raycastOffsetX = 0.4f;
    public float slideSpeed = 4f;
    public GameObject Over;
    //public AudioClip Footsteps;
    //public AudioSource FootstepsSource;

    
    //TESTING ! @ # ! @#!@ #! @#!@ # 

    //The two colliders for running and sliding
    public CapsuleCollider2D colRunning;
    public CapsuleCollider2D colSliding;

    public Animator anim;

    //Set direction to right.
    public Vector3 userDirection = Vector3.right;

    private Rigidbody2D charRB;

    void Start()
    {
        //Get the reference to the rigidbody component and animator in Game Object
        //FootstepsSource.clip = Footsteps;
        charRB = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        //FootstepsSource.Play();

    }

    // Update is called once per frame
    void Update()
    {


        //Set sliding to false at default.
        colSliding.enabled = false;
        colRunning.enabled = true;

        //Move the character right at a speed.
        if (ScoreManager.stamina > 0)
        {
            //This works better than a translate because it's actually moving the character.
            charRB.velocity = new Vector2(speed, charRB.velocity.y);


        }

        if (ScoreManager.stamina < 26 && ScoreManager.stamina > 20)
        {
            speed = 11f;
        }

        if (ScoreManager.stamina <= 20 && ScoreManager.stamina >= 16)
        {
            speed = 10f;
        }

        if (ScoreManager.stamina <= 15 && ScoreManager.stamina >= 11)
        {
            speed = 9f;
        }

        if (ScoreManager.stamina <= 10 && ScoreManager.stamina >= 6)
        {
            speed = 8.5f;
        }

        //else if (ScoreManager.stamina < 1)
        //{
        //    Die();
        //    anim.SetInteger("State", 3);
        //}





            //New bool isGrounded is function bool isGrounded.
            bool isGrounded = IsGrounded(raycastOffsetX);


        //Jump.
        if (colSliding.enabled == false && isGrounded && colRunning.enabled == true)
        {
            if (ScoreManager.stamina > 0)
            {
                if (Input.GetKey(KeyCode.Space))
                { Jump(); }
            }
        }

        //Set sliding collider.
        if (isGrounded)
        {

            if (ScoreManager.stamina > 0)
            {
                if (Input.GetKey(KeyCode.S))
                {
                    anim.SetInteger("State", 1);

                    Slide();

                }

                else
                {
                    anim.SetInteger("State", 0);
                }
            }



            if (ScoreManager.stamina < 1)
            {
                anim.SetInteger("State", 3);
                Die();
            }
            }

            if (!isGrounded)
        {
            anim.SetInteger("State", 2);
        }

        
    }



    //Check if the character is grounded.
    bool IsGrounded(float offsetX)
    {
        //Get the characters pivot point and offset in X
        Vector2 origin = transform.position;
        origin.x += offsetX;

        //Raycast from that point in world.
        Debug.DrawRay(origin, Vector3.down * raycastDistance);

        //Check if the raycast hit.
        RaycastHit2D hitInfo = Physics2D.Raycast(origin, Vector2.down, raycastDistance);

        //Return true or false.
        return hitInfo;
    }

    //Set the character's jump velocity.
    private void Jump()
    {
        //Jump, man, jump.
        charRB.velocity = new Vector2(charRB.velocity.x, jumpSpeed);

    }

    public void Slide ()
    {
        //set the sliding collider, disable the running collider.
        colSliding.enabled = true;
        colRunning.enabled = false;

    }

    public void Die()
    {
        //freeze velocity.
        charRB.velocity = new Vector2(0, charRB.velocity.y);

        //Game over screen.
        Over.SetActive(true);




        //Make the character slide.
        anim.SetInteger("State", 3);
        //FootstepsSource.enabled = false;

        //charRB.velocity = new Vector2(0, 0);

    }

}
