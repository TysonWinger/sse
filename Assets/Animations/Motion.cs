﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motion : MonoBehaviour {

    public Camera Cam;
    public float Speed;
	
	// Update is called once per frame
	void Update () {
        Cam.transform.Translate(Vector3.right * Time.deltaTime * Speed);
	}
}
